package com.example.noteapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.adapters.NoteRecycleAdapter
import com.example.noteapp.databinding.FragmentNoteBinding
import com.example.noteapp.viewmodel.NoteViewModel

class NoteFragment : Fragment() {

    private var _binding: FragmentNoteBinding? = null
    private val binding get() = _binding!!
    private val noteViewModel by viewModels<NoteViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNoteBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvNote.layoutManager = LinearLayoutManager(context)

        binding.btnNote.setOnClickListener {
            val input = binding.etNote.text.toString()
            noteViewModel.addToNote(input)

            binding.rvNote.adapter = NoteRecycleAdapter().apply {
                noteViewModel.getNote()
                noteViewModel.note.observe(viewLifecycleOwner) {
                    addNoteItem(it)
                }
            }

//            binding.rvNote.adapter = NoteRecycleAdapter().apply {
//                val input = binding.etNote.text.toString()
//                noteViewModel.addToNote(input)
//                noteViewModel.note.observe(viewLifecycleOwner) {
//                    addNoteItem(it)
//                }
//            }
        }
    }

}