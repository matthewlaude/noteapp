package com.example.noteapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.R
import com.example.noteapp.adapters.ShowRecycleAdapter
import com.example.noteapp.databinding.FragmentNoteBinding
import com.example.noteapp.databinding.FragmentShowBinding


class ShowFragment : Fragment() {
    private var _binding: FragmentShowBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShowBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: ShowFragmentArgs by navArgs()
        val item = args.word
        val splitString = item.split("")?.filter { str -> str.isNotEmpty() }?.toList()

        binding.rvCharList.layoutManager = LinearLayoutManager(context)
        binding.rvCharList.adapter = ShowRecycleAdapter().apply {
            splitString?.let { addShowItem(it) }
        }
    }
}