package com.example.noteapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.noteapp.model.NoteRepo
import kotlinx.coroutines.launch

class NoteViewModel : ViewModel() {
//    private val repo = NoteRepo
    private var tempNote = mutableListOf<String>()

    private val _note = MutableLiveData<List<String>>()
    val note : LiveData<List<String>> get() = _note

    fun getNote() {
        viewModelScope.launch {
            val itemNote = tempNote
            _note.value = itemNote
        }
    }
    fun addToNote(input: String) {
        tempNote.add(input)
    }
}