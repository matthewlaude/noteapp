package com.example.noteapp.model

import com.example.noteapp.model.remote.NoteApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object NoteRepo {

    private val NoteApi = object : NoteApi {
        override suspend fun getNote(): List<String> {
            return mutableListOf()
        }
    }

//    suspend fun getNote(): List<String> = withContext(Dispatchers.IO) {
//        NoteApi.getNote()
//    }
}