package com.example.noteapp.model.remote

interface NoteApi {
    suspend fun getNote(): List<String>
}