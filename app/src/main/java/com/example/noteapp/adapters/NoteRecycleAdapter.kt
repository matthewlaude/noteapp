package com.example.noteapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemNoteBinding
import com.example.noteapp.view.NoteFragmentDirections

class NoteRecycleAdapter : RecyclerView.Adapter<NoteRecycleAdapter.NoteViewHolder>() {

    private var note = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val binding = ItemNoteBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return NoteViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val noteItem = note[position]
        holder.loadNote(noteItem)
    }

    override fun getItemCount(): Int {
        return note.size
    }

    fun addNoteItem(note: List<String>) {
        this.note = note.toMutableList()
        notifyDataSetChanged()
    }

    class NoteViewHolder(
        private val binding: ItemNoteBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadNote(noteItem: String) {
            binding.tvNote.text = noteItem
            binding.tvNote.setOnClickListener {
                it.findNavController()
                    .navigate(NoteFragmentDirections.actionNoteFragmentToShowFragment(noteItem))
            }
        }
    }
}