package com.example.noteapp.adapters

import android.text.Layout
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemShowBinding

class ShowRecycleAdapter : RecyclerView.Adapter<ShowRecycleAdapter.ShowViewHolder>() {

    private var show = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        val binding = ItemShowBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ShowViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        val showItem = show[position]
        holder.loadShow(showItem)
    }

    override fun getItemCount(): Int {
        return show.size
    }

    fun addShowItem(show: List<String>) {
        this.show = show.toMutableList()
        notifyDataSetChanged()
    }

    class ShowViewHolder(
        private val binding: ItemShowBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun loadShow(showItem: String) {
            binding.tvChar.text = showItem
        }
    }
}